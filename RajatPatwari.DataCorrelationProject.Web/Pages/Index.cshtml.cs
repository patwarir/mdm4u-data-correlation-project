﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;

namespace RajatPatwari.DataCorrelationProject.Web.Pages
{
    public sealed class IndexModel : PageModel
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public IndexModel(IWebHostEnvironment hostingEnvironment) =>
            _hostingEnvironment = hostingEnvironment;

        public void OnGet() =>
            ViewData["data"] = new SortedDictionary<string, (int, double)>(Program.GetData(_hostingEnvironment));
    }
}