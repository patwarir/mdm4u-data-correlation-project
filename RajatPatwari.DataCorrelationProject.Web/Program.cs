﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;

namespace RajatPatwari.DataCorrelationProject.Web
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.White;
            Console.Write("Mr. Garrison");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(", if you are seeing this, please go to the url that the application shows.");
            Console.WriteLine("For example, if the application states 'Now listening on: http://localhost:5000', please go to that link on your browser/Chrome.");
            Console.WriteLine("I could not sign this because a signing certificate costs like $500 a month, but if the browser says untrusted, please go on.");
            Console.WriteLine("I do not have any malware on this; I promise. I would have nothing to gain and everything to lose.");
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());

        public static IDictionary<string, (int, double)> GetData(IWebHostEnvironment hostingEnvironment)
        {
            var path = Path.Combine(hostingEnvironment.WebRootPath, "data", "data.csv");
            var lines = File.ReadAllLines(path);

            var data = new Dictionary<string, (int, double)>();

            foreach (var line in lines)
            {
                var country = line.Remove(line.IndexOf(','));

                var perCapTemp = line.Substring(line.IndexOf(',') + 1);
                var perCap = Convert.ToInt32(perCapTemp.Remove(perCapTemp.LastIndexOf(',')));

                var hdi = Convert.ToDouble(line.Substring(line.LastIndexOf(',') + 1));

                data.Add(country, (perCap, hdi));
            }

            return data;
        }
    }
}